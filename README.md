# Simplecss

Simplecss is a simple css framework created for personal use. It is based on [ITCSS (Inverted Triangle CSS)](https://www.xfive.co/blog/itcss-scalable-maintainable-css-architecture/) architecture and [BEM methodology](https://en.bem.info/methodology/).

## Prerequisites

- [Gulp](https://gulpjs.com/)
- [Npm](https://www.npmjs.com/)

## Quick start

- Clone the repo: `git clone https://github.com/twbs/bootstrap.git`
- Install dev dependencies with [npm](https://www.npmjs.com/): `npm install`
- Run gulp: `gulp`