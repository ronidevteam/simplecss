var gulp = require('gulp');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var sass = require('gulp-sass');
sass.compiler = require('node-sass');


/**
* Build index file. Add header, followed by all partial files, followed by the footer.
*/
gulp.task('html', function(){
  return gulp.src([
      'src/assets/html/header.html',
      'src/assets/html/partials/*.html',
      'src/assets/html/footer.html'])
    .pipe(concat('index.html'))
    .pipe(gulp.dest('public/'))
});



/**
* Convert scss to css
*/
gulp.task('sass', function () {
  return gulp.src('./src/assets/scss/index.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('public/'));
});



/**
* Bundle js
*/
gulp.task('js', function () {
  return gulp.src('./src/assets/js/app.js')
    .pipe(gulp.dest('public/'));
});


/**
* Watch for changes in any html file in partials directory. If any - run html task
*/
gulp.task('watch', function () {
  gulp.watch('src/assets/html/**/*.html', ['html']);
  gulp.watch('src/assets/scss/**/*.scss', ['sass']);
  gulp.watch('src/assets/js/**.js', ['js']);
});


/**
 * Default task. Runs all necessary tasks
 */
gulp.task('default', [ 'html', 'sass', 'js', 'watch']);