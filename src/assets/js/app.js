(function () {
    var $buttons = $('.js-switch-section');
    var $windows = $('.js-wdw');
    var $windowTitle = $('.js-wdw-title');

    function switchSection(event) {
        event.preventDefault();

        // Hide all sections
        $windows.css('display', 'none');

        // Chosen section name
        var sectionName = $(event.target).data('target');

        // Show choosen section
        $('*[data-section="' + sectionName +'"]').css('display', 'block');

        var sectionTitle = sectionName.replace('wdw-', '');
        $windowTitle.text(sectionTitle);
    }
    $buttons.on('click', switchSection);

})();